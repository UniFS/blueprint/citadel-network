# Perfect Example!

## ORICO DS500U3-US-BK
price: $170 or 34/disk
retail: https://www.newegg.com/orico-ds500u3-us-bk-dock/p/0VN-0003-00182

### Cost
```
E='ORICO DS500U3-US-BK'
priceE=170


# with 10TB drive:
n=10
priceN=225
(priceN*5+priceE)/(n*10)
= 25.9/T
= 647.5/25T equivalent

# with 8TB drive:
n=8
priceN=152
(priceN*5+priceE)/(n*8)
= 23.25/T


# with 6TB drive:
n=6
priceN=100
(priceN*5+priceE)/(n*10)
= 22.33/T  (100*5+170)/30

# https://www.newegg.com/p/pl?N=100167523%20600376735&Order=PRICE
```